def m1(n):
    """
    Parameters
    ==========
    n : int

    Return
    ======
    numpy.array[[1, 1, 1, ... , 1],
                [1, 2, 2, ... , 2],
                [1, 2, 3, ... , 3],
                 ................
                [1, 2, 3, ... , n]]

    """
    import numpy as np

    if n < 1:
        raise ValueError("n should be positive integer")

    matrix = np.empty([n, n], dtype=int)

    for i in range(n):
        matrix[i, i:] = i + 1
        matrix[i:, i] = i + 1

    return matrix


def m2(n, x):
    """
    Parameters
    ==========
    n : int
    x : float

    """
    import numpy as np

    if n < 1:
        raise ValueError("n should be positive integer")

    matrix = np.eye(n, k=1)
    factor = 1.0

    for i in range(n):
        factor *= x/(i+1.0)
        matrix += np.eye(n, k=-i) * factor

    return matrix


def m3(x):
    """
    Parameters
    ==========
    x : list

    Return
    ======
    numpy.array[[0, ... , 0, x_1],
                [0, ... , 0, x_2],
                 ...............
                [x_1,x_2,...,x_n]]

    """
    import numpy as np

    if len(x) < 1:
        raise ValueError("x can't be empty")

    matrix = np.zeros([len(x), len(x)], dtype=int)
    matrix[:, -1] = x
    matrix[-1, :] = x

    return matrix


def m4(x):
    """
    Parameters
    ==========
    x : list

    """
    import numpy as np

    if len(x) < 1 or len(x)%2 == 0:
        raise ValueError("x must contain odd number of elements.")

    n = int(len(x)/2) + 1

    matrix = np.empty([n, n])

    for i in range(n):
        matrix[:, i] = x[i:i+n][::-1]

    return matrix


def m5(x):
    """
    Parameters
    ==========
    x : list

    """
    import numpy as np
    return np.flipud(m4(x))


def m6(n):
    """
    Parameters
    ==========
    n : int

    Return
    ======
    A : numpy.array
        A[i, j] is equal to gcd(i, j).

    """
    import numpy as np
    from itertools import product
    from sage.all import gcd

    ij = list(product(range(1, n+1), range(1, n+1)))

    return np.matrix(list(map(gcd, ij))).reshape(n, n)


def det_m6(n):
    """
    Parameters
    ==========
    n : int

    """
    import numpy as np
    from sage.all import euler_phi

    eulers = list(map(euler_phi, range(2, n+1)))

    return np.prod(eulers)


def prod(A, B):
    """
    Return tensor product of matrices ``A`` and ``B``.

    Parameters
    ==========
    A : array-like, shape=[m, n]
    B : array-like, shape=[r, s]

    Examples
    ========
    >>> from l20150311 import prod
    >>> A = [[1, 2], [3, 4]]
    >>> B = [5, 6]
    >>> prod(A, B)
    array([[ 5,  6, 10, 12],
           [15, 18, 20, 24]])

    """
    import numpy as np
    A = np.array(A)
    B = np.array(B)

    if A.ndim == 1:
        m, n = (1, len(A))
    else:
        m, n = A.shape

    if B.ndim == 1:
        r, s = (1, len(B))
    else:
        r, s = B.shape

    rows = []

    for i in range(m):
        rows.append(np.hstack([a*B for a in A[i, :]]))

    return np.vstack(rows)

