def is_skew_symmetric(m):
    """
    Check if the matrix ``m`` is skew-symmetric.

    Parameters
    ==========

    m : array-like, shape=[n, n]

    """
    import numpy as np
    m = np.matrix(m)
    return np.array_equal(-1*m, m.transpose())


def _sort_count(A):
   l = len(A)
   if l > 1:
      n = l//2
      C = A[:n]
      D = A[n:]
      C, c = _sort_count(A[:n])
      D, d = _sort_count(A[n:])
      B, b = _merge_count(C,D)
      return B, b+c+d
   else:
      return A, 0


def _merge_count(A,B):
   count = 0
   M = []
   while A and B:
      if A[0] <= B[0]:
         M.append(A.pop(0))
      else:
         count += len(A)
         M.append(B.pop(0))
   M  += A + B
   return M, count


def sign(permutation):
    """
    Return the signature of a permutation.

    Parameters
    ==========
    permutation : list of integers

    """
    invs = _sort_count(permutation)[1]
    return 1 if invs%2 else -1


def pf(m, verbose=False):
    """
    Parameters
    ==========

    m : array-like, shape=[2n, 2n]
    verbose : bool

    Examples
    >>> from l20150325 import pf
    >>> pf([[0,1,1,1], [-1,0,1,1], [-1,-1,0,1], [-1,-1,-1,0]])
    1
    >>> pf([[0, 4], [-4, 0]])
    4

    """
    if not is_skew_symmetric(m):
        raise ValueError("``m`` must be skew-symmetric")

    import numpy as np
    m = np.matrix(m)

    if m.shape[0] % 2 == 1:
        raise ValueError("``m`` must have even order")

    from itertools import combinations, chain
    n = m.shape[0]/2
    result = 0

    from sage.all import SetPartitions
    partitions = SetPartitions(range(1, 2*n+1), [2]*n).list()

    for partition in partitions:
        if verbose:
            print(
                partition,
                sign(partition.to_permutation()),
                partition.to_permutation().sign()
            )
        p = partition.standard_form()
        p = list(map(lambda x: (x[0]-1, x[1]-1), p))
        tmp = np.prod(list(map(m.item, p)))
        tmp *= sign(partition.to_permutation())
        result += tmp

    return result


def continuant(a):
    from sage.all import SR, matrix
    m = matrix(SR, len(a), lambda i, j: i-j if abs(i-j)==1 else 0)
    m += matrix(SR, len(a), lambda i, j: a[i] if i==j else 0)

    return m.det()


def ratio(a):
    if len(a) == 1:
        return a[0]

    return a[0] + 1.0/ratio(a[1:])


def det_l_k(m):
    from sage.all import SR, matrix
    n = m.nrows()
    M = matrix(SR, n-1, n-1, lambda i, j: matrix(SR, 2, 2, lambda k, l: m[i + k if k else 0][j + l]).det())

    prod = 1
    for i in xrange(n - 2):
         prod *= m[0, i + 1]

    return M.det()/prod


def det_inv(M, P, Q, verbose=False):
    """
    Parameters
    ==========

    M : array-like, shape=[n, n]
    P : list of integers in {1, ... , n}
    Q : list of integers in {1, ... , n}

    Examples
    ========
    >>> from sage.all import matrix, SR, var
    >>> from l20150325 import det_inv
    >>> M = matrix(SR, 3, 3, lambda i, j: var('x'+str(i)+str(j)))
    >>> P = [1, 2]
    >>> Q = [1, 3]
    >>> det_inv(M, P, Q, verbose=True)

    """
    from itertools import product
    from sage.all import matrix
    import numpy as np

    if M.nrows() != M.ncols():
        raise ValueError("M must be square matrix")
    n = M.nrows()

    nP = set(range(1, n+1)).difference(set(P))
    nQ = set(range(1, n+1)).difference(set(Q))
    nPxnQ = list(product(list(nP), list(nQ)))
    nPxnQ = list(map(lambda x: (x[0]-1, x[1]-1), nPxnQ))

    PxQ = list(product(P, Q))
    PxQ = list(map(lambda x: (x[0]-1, x[1]-1), PxQ))

    Mpq = np.matrix(M.inverse())
    Mpq = np.matrix(list(map(Mpq.item, PxQ))).reshape(len(P), len(Q))
    Mpq = matrix(Mpq.tolist())

    Mnpnq = np.matrix(M)
    Mnpnq = np.matrix(list(map(Mnpnq.item, nPxnQ))).reshape(len(nP), len(nQ))
    Mnpnq = matrix(Mnpnq.tolist())

    #TODO: complete function

    return

